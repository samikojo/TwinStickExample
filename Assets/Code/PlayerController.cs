using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GA.TwinStick.Input;
using System;

namespace GA.TwinStick
{
	public class PlayerController : MonoBehaviour
	{
		[SerializeField] private float _speed = 1;
		[SerializeField] private float _turningSpeed = 45;

		private Controls _controls;
		private Vector2 _move;
		private Vector2 _look;

		private void Awake()
		{
			_controls = new Controls();
		}

		private void OnEnable()
		{
			_controls.Game.Enable();
		}

		private void OnDisable()
		{
			_controls.Game.Disable();
		}

		private void Update()
		{
			ReadInput();
			Move();
			Turn();
		}

		private void Turn()
		{
			transform.RotateAround(transform.position, transform.up, _look.x * _turningSpeed * Time.deltaTime);
		}

		private void Move()
		{
			Vector2 movement = _move * _speed * Time.deltaTime;
			transform.Translate(new Vector3(movement.x, 0, movement.y), Space.Self);
		}

		private void ReadInput()
		{
			_move = _controls.Game.Move.ReadValue<Vector2>();
			_look = _controls.Game.Look.ReadValue<Vector2>();
		}
	}
}